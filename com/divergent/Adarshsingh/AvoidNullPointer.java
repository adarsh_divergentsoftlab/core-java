package com.divergentsl.adarshsingh;

public class AvoidNullPointer {
	static int value = 50;  
	public static int getValue(String s) {  
	if(s == null && value == 50) {  
	throw new IllegalArgumentException("Arguments can not be null");  
	}  
	return value;  
	}  
	public static void main(String[] args) {  
	String s = null;  
	try {  
	System.out.println(getValue(s));  
	}catch (IllegalArgumentException ex) {  
	// TODO: handle exception  
	System.out.println("IllegalArgumentException caught");  
	}  
	  
	s = "JTP";  
	try {  
	System.out.println(getValue(s));  
	}catch (IllegalArgumentException ex) {  
	// TODO: handle exception  
	System.out.println("IllegalArgumentException caught");  
	}  
	  
	}  

}
