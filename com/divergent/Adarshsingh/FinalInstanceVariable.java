package com.divergentsl.adarshsingh;

public class FinalInstanceVariable {
	public static void main(String[] args) 
    {
        // a final reference variable sb
        final StringBuilder sb = new StringBuilder("Adarsh Singh");
          
        System.out.println(sb);
          
        // changing internal state of object
        // reference by final reference variable sb
        sb.append("Adarsh Singh");
          
        System.out.println(sb);
    }    

}
