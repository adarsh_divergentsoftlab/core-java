package com.divergentsl.adarshsingh;

public class FInalStaticVariable {
	static String company = "Divergent";
    String name;
    int rollno;
public
    static void main(String[] args)
    {
	FInalStaticVariable  ob = new FInalStaticVariable();
  
        // If we create a database for GFG org
        // then the company name should be constant
        // It can�t be changed by programmer.
        ob.company = "Divergent Software Lab";
  
        ob.name = "Bishal";
        ob.rollno = 007;
        System.out.println(ob.company);
        System.out.println(ob.name);
        System.out.println(ob.rollno);
    }

}
